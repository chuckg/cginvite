--[[

cgInvite 1.5.5

Author: chuckg
Email: c@chuckg.org
URL: chuckg.wowinterfaces.com

1.0 - 	Added a GUI with some basic wait list functionality.
1.1 - 	Ability to setup an auto response, no ability to change the text.
		Added a clear wait list button.
1.2 - 	Added better raid limit checks plus an automatic conversion to raid if your limit is over 5; don't cry if this shit breaks w/ raid convert lag.
1.3 - 	Fixed the XML to work with the new standards of 1.10 and updated the TOC.
1.4 - 	Cleaned up a few lines of code and fixed a typo.
1.5 - 	Massive code cleanup, event fixes, and optimizations.
			1. Added guild invites only.
			2. Added message to notify individuals if they're already in a group.
			3. Slash command to enable/disable
			4. Version .5 - fixed up the "drop group"

]]--

-- Configuration Variables
CGInviteVersion = "1.5.5";
CGInviteEnable = false;
CGInviteConfig = {};

CGInviteWaitList = {};
CGInviteLastWhispered = nil;

function CGInvite_Initialize()
	CGInviteConfig.version = CGInviteVersion;
	CGInviteConfig.keyword = "invite";
	CGInviteConfig.raidlimit = 0;
	CGInviteConfig.autolist = true;
	CGInviteConfig.autorespond = false;
	CGInviteConfig.autorespond_text = "You have been added wait list and are currently #$$ in line.";
	CGInviteConfig.guildonly = false;
	CGInviteConfig.guildonly_text = "Only taking invite requests from guild members.";
	CGInviteConfig.grouped_text = "Drop your group before you request an invite.";
	CGInviteConfig.waitlist = "";
end

function CGInvite_OnLoad()
	SlashCmdList["CGI"] = CGInvite_CmdLine;
	SLASH_CGI1 = "/cgi"; 
	
	-- Bit hacky, but apparently you can't register events on the fly, or they can't be registered period.
	this:RegisterEvent("VARIABLES_LOADED");

	if (DEFAULT_CHAT_FRAME) then
		CGInvite_PrintD("CGInvite loaded, type /cgi to get to the options menu.");
	end
end

function CGInvite_OnEvent(event)
	-- Register remaining events we'll need, initialiaze the variables for the first time and/or update new ones.
	if (event == "VARIABLES_LOADED") then
		-- Register
		CGInvite_Enable(false);
		-- Don't need this anymore.
		this:UnregisterEvent("VARIABLES_LOADED");
		if (not CGInviteConfig or CGInviteConfig.version ~= CGInviteVersion) then
			CGInvite_Initialize();
		end
	end

	if (not CGInviteEnable) then
		return;
	end
	
	if (event == "CHAT_MSG_WHISPER") then
		if ((CGInviteConfig.keyword == nil or string.find(string.lower(arg1), CGInviteConfig.keyword))) then
			-- Inform the player we're doing guild invites only.
			if (CGInviteConfig.guildonly and not CGInvite_PlayerInGuild(arg2)) then
				CGInvite_WhisperPlayer(CGInviteConfig.guildonly_text, arg2);
			else
				-- Make sure we're not over our limit
				if (CGInvite_CheckLimit()) then
					-- Invite the person
					CGInvite_Invite(arg2);
					CGInviteLastWhispered = arg2
				elseif (CGInviteConfig.autolist) then
					-- Put the person the list
					CGInvite_WaitAdd(arg2);
				end
			end
		end
	elseif (CGInviteLastWhispered and event == "CHAT_MSG_SYSTEM") then
		if (string.find(arg1, "(.+) has joined the raid group") or string.find(arg1, "(.+) joins the party.") or string.find(arg1, "(.+) declines your group invitation.")) then
			CGInviteLastWhispered = nil;
		elseif ( CGInviteLastWhispered ~= nil and string.find(arg1, "(.+) is already in a group.")) then
			CGInvite_WhisperPlayer(CGInviteConfig.grouped_text, CGInviteLastWhispered);
			CGInviteLastWhispered = nil;
		end
	end
end

function CGInvite_Enable(toggle)
	if (toggle) then
		CGInviteEnable = true;
	end
	
	-- Register the events on load since we can't do it on the fly.
	this:RegisterEvent("CHAT_MSG_WHISPER");
	this:RegisterEvent("CHAT_MSG_SYSTEM");
end

function CGInvite_Disable(toggle)
	if (toggle) then
		CGInviteEnable = false;
	end
	
	this:UnregisterEvent("CHAT_MSG_WHISPER");
	this:UnregisterEvent("CHAT_MSG_SYSTEM");
end

function CGInvite_SetWaitList()
	local player;
	CGInviteWaitList = {};
	for player in string.gfind(CGInviteConfig.waitlist, "[^\n]+") do
		if (player and player ~= "") then
			CGInviteWaitList[getn(CGInviteWaitList) + 1] = player;
			local temp = getn(CGInviteWaitList)+1;
		end
	end
end

function CGInvite_GetWaitList()
	local index;
	CGInviteConfig.waitlist = "";
	for index in CGInviteWaitList do
		CGInviteConfig.waitlist = CGInviteConfig.waitlist..CGInviteWaitList[index].."\n";
	end
end

function CGInvite_WaitAdd(who)
	local found = false;
	for index in CGInviteWaitList do
		if (CGInviteWaitList[index] == who) then
			found = index;
		end
	end
	
	if (not found) then
		CGInviteWaitList[table.getn(CGInviteWaitList) + 1] = who;
		-- 1.1: Update the editbox on the fly
		CGInvite_GetWaitList();
		CGInvite_SetWaitList();
		CGInviteWaitListEditFrameEditBox:SetText(CGInviteConfig.waitlist);
		if (CGInviteConfig.autorespond) then
			CGInvite_WhisperPlayer(string.gsub(CGInviteConfig.autorespond_text, "($$)", table.getn(CGInviteWaitList)), who);
		end
	elseif (found and CGInviteConfig.autorespond) then
		CGInvite_WhisperPlayer("You're already #" .. found .." on the wait list.", who);
	end
end

function CGInvite_WaitInvite()
	if (table.getn(CGInviteWaitList) > 0) then
		CGInvite_PrintD("|cffff0000Inviting: |cffffff00"..CGInviteWaitList[1]);
		InviteByName(CGInviteWaitList[1]);
		table.remove(CGInviteWaitList, 1);
	end
end

function CGInvite_Invite(who) 
	if (GetNumRaidMembers() == 0 and ((GetNumPartyMembers()+1) < CGInviteConfig.raidlimit and (CGInviteConfig.raidlimit > 5 or CGInviteConfig.raidlimit == 0))) then
		-- Can't create a raid till we have some party members to begin with.
		-- We're not in a raid yet AND our limit is over the max party size, so we'll convert to raid.
		ConvertToRaid();
	end

	InviteByName(who, true);
end

function CGInvite_PlayerInGuild(who)
	local guildindex;

	-- Update the guild list.
	GuildRoster();
	for guildindex=1, GetNumGuildMembers() do
		local name, _, _, _, _, _, _, _, _, _ = GetGuildRosterInfo(guildindex);
		if ( who == name ) then
			return true;
		end
	end
	
	return false;
end

function CGInvite_CheckLimit() 
	if (CGInviteConfig.raidlimit == 0 and GetNumRaidMembers() < 40) then
		return true;
	elseif (GetNumRaidMembers() == 0 and (GetNumPartyMembers()+1) < CGInviteConfig.raidlimit) then
		return true;
	elseif (GetNumRaidMembers() ~= 0 and GetNumRaidMembers() < CGInviteConfig.raidlimit) then
		return true;
	else
		return false;
	end
end

function CGInvite_CmdLine(param)
	local arg1;
	local arg2;
	_, _, arg1, arg2 = string.find(param, "(%a+)%s+(%w+)");
	if (not arg1) then
		arg1 = param;
	end
	
	if (arg1 == "list") then
		CGInvite_PrintWaitList();
	elseif (arg1 == "enable") then
		if ( not CGInviteEnable and not CGInviteEnableCheckButton:GetChecked() ) then
			CGInviteEnableCheckButton:SetChecked(1);
			CGInvite_PrintD("|cff00ff00CGInvite enabled.");
		end
		CGInvite_Enable(true);
	elseif (arg1 == "disable") then
		if ( CGInviteEnable and CGInviteEnableCheckButton:GetChecked() ) then
			CGInviteEnableCheckButton:SetChecked(0);
			CGInvite_PrintD("|cffff0000CGInvite disabled.");
		end
		CGInvite_Disable(true);
	elseif (CGInviteOptionsFrame:IsVisible()) then
		CGInviteOptionsFrame:Hide();
	else
		CGInviteOptionsFrame:Show();
	end
end

function CGInvite_WhisperPlayer(msg, who)
	SendChatMessage(msg, "WHISPER", nil, who);
end

function CGInvite_PrintD(msg)
	DEFAULT_CHAT_FRAME:AddMessage(msg);
end

function CGInvite_PrintWaitList()
	local index;
	for index in CGInviteWaitList do
		CGInvite_PrintD(" - "..CGInviteWaitList[index]);
	end
end
