## Interface: 11000
## Title: cgInvite
## Notes: Automatically invite people who send you a tell with a keyword and has an auto wait list feature.
## Version: 1.5.5
## Author: Blindchuck <Ropetown>
## SavedVariables: CGInviteConfig, CGInviteEnable
## OptionalDeps: 
## Dependencies: 
cgInvite.xml